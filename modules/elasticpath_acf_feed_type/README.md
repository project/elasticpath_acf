# Elastic Path ACF Feed Type

This module provides configuration for a set of Drupal [Feeds](https://www.drupal.org/project/feeds). These allow site admins to import [categories](https://documentation.elasticpath.com/commerce-cloud/docs/api/catalog/categories/index.html), [products](https://documentation.elasticpath.com/commerce-cloud/docs/api/catalog/products/index.html), and [product images](https://documentation.elasticpath.com/commerce-cloud/docs/api/catalog/products/relationships/main-image-relationship.html) from Elastic Path Commerce Cloud.

It uses the Fetchers by `elasticpath_acf_feeds` and the JsonPath parser provided by the [Feeds Extensible Parsers](https://www.drupal.org/project/feeds_ex) module. The imported products are mapped to content nodes with the type Product, which is provided by [ACF](https://github.com/acquia/acf/).

## Note on Import Order
In order to successfully import, the Category and Image importers must be run before the Product importer. This is because ACF's Product content type marks both the category and image fields as required, and feeds does not support importing content with complex dependency graphs.

> **Aside**: Feeds was chosen as a basis for the importer because it is visual (making it easy to demo), fairly flexible (it works well with Elastic Path Commerce Cloud's flexible data extensions), and relatively easy to work with.
