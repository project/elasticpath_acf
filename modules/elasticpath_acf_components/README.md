# Elastic Path ACF Components

This module contains components(1) which override [ACF](https://github.com/acquia/acf/)'s service layer(2). It provides an alternate implementation of ACF's Cart Manager, but re-uses ACF's existing Storage and Event Emitter components.

## Code Map

- `libraries/elasticpath_acf_cart_manager` contains the code for the Elastic Path cart manager. This provides the bulk of the front-end functionality for the integration.
- `plugins/elasticpath_acf_core` is responsible for:
  1. Providing an Elastic Path Commerce Cloud plugin(3) for ACF
  2. Instantiating the front-end code used by the Elastic Path ACF service layer.

## Footnotes

1. In this context, "Components" refers to a feature provided by the Drupal [Component module](https://www.drupal.org/project/component).
2. The ACF's service layer is a set of JavaScript objects that are instantiated on a page. They act as a proxy for whatever commerce back-end is being used by ACF at the time, and allow commerce platforms to provide alternate implementations.
3. In this context, plugin refers to the Component(1) module's plugin functionality.