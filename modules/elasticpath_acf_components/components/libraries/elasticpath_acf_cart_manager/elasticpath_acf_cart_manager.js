const ElasticPathACFCartManager = (function() {

  /**
   * @constant string Carts endpoint in Elastic Path Commerce Cloud.
   */
  const CARTS_ENDPOINT = '/v2/carts';

  /**
   * @constant string Event key for add-to-cart events.
   */
  const ADD_TO_CART_EVENT_KEY = 'acfAddToCart';

  /**
   * @constant string Event key for remove-from-cart events.
   */
  const REMOVE_FROM_CART_EVENT_KEY = 'acfRemoveFromCart';

  /**
   * @constant string Event key for cart update events.
   */
  const EMPTY_CART_EVENT_KEY = 'acfEmptyCart';

  /**
   * @constant string Event key for cart update events.
   */
  const CART_UPDATE_EVENT_KEY = 'acfUpdateCart';

  /**
   * @constant string LocalStorage key for the promo code.
   */
  const PROMOCODE_LOCALSTORAGE_KEY = 'elasticPathAcfPromoCode';

  /**
   * Returns a cart ID for Elastic Path Commerce Cloud.
   *
   * @returns string
   *   Commerce Cloud cart ID.
   */
  function getElasticPathCartId() {
    const CART_ID_KEY = 'acf.elasticpathcommercecloud.cartid';

    let cartId = localStorage.getItem(CART_ID_KEY);

    if (cartId === undefined || cartId === null) {
      cartId = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'.replace(
        /[x]/g,
        () => ((Math.random() * 16) | 0).toString(16)
      );

      localStorage.setItem(CART_ID_KEY, cartId);
    }

    return cartId;
  }

  /**
   * Creates a set of functions for a manually invalidatable memoize function.
   *
   * @returns object isValid, markValid and invalidate functions.
   */
  function createManuallyInvalidatableCacheValidator() {
    const AGE = Symbol.for('__AGE__');
    let oldestValidRecord = Date.now();

    return {
      isValid: (entry) => {
        return entry[AGE] && entry[AGE] >= oldestValidRecord
      },

      markValid: (entry) => {
        entry[AGE] = Date.now();
      },

      invalidate: () => {
        oldestValidRecord = Date.now();
      },
    };
  }

  function formatPrice(elasticPathPriceAmount) {
    return `${parseFloat(elasticPathPriceAmount / 100).toFixed(2)}`;
  }

  /**
   * Transforms an Elastic Path cart item to the ACF specification.
   *
   * @param object item Elastic Path Commerce Cloud cart item.
   * @returns object ACF Cart Item.
   */
  async function transformElasticPathCartItemToAcf(item) {
    const drupalNode = await elasticPathACFUtils.getDrupalNodeByElasticPathSku(item.sku);
    return {
      id: item.id,
      productId: drupalNode.attributes.drupal_internal__nid,
      quantity: asQuantity(item.quantity),
      displayPrice: item.meta.display_price.without_tax.value.formatted,
    };
  }

  /**
   * Parses a string or number as a quantity (non-negative integer), throwing
   * an error if result is invalid.
   */
  function asQuantity(value) {
    let output;
    if (typeof value === 'number') {
      output = value;
    } else if (typeof value === 'string') {
      output = Number.parseFloat(value);
    } else {
      throw new Error(`Provided value is of type ${typeof value}. Must be either number or string.`);
    }

    if (output % 1 !== 0) {
      throw new Error(`Provided value is a float. Must be an integer.`);
    }

    if (value < 0) {
      throw new Error(`Valid quantities must be non-negative. You provided ${output}`);
    }

    return output;
  }

  /**
   * No cart item matches the provided Id.
   */
  class NoSuchCartItemError extends Error {
    constructor(msg) {
      super(msg);
      this.name = 'NoSuchCartItemError';
    }
  }

  /**
   * No product matches the provided Id.
   */
  class NoSuchProductError extends Error {
    constructor(msg) {
      super(msg);
      this.name = 'NoSuchProductError';
    }
  }

  /**
   * Interface for all interactions with the cart.
   *
   * @param EventEmitter eventEmitter ACF's event emitter.
   * @constraint At any given time, each CartItemId must map to at most one CartItem.
   */
  return class ElasticPathACFCartManager {

    constructor(eventEmitter) {
      ACF.cart = {
        products: {},
        promoCode: "",
        amount: {
          subtotal: "0.00",
          discount: "0.00",
          tax: "0.00",
          shipping: "",
          total: "0.00"
        },
      };

      const {
        isValid,
        markValid,
        invalidate,
      } = createManuallyInvalidatableCacheValidator();

      this._getElasticPathCart = elasticPathACFUtils.memoize(
        this._getElasticPathCart,
        isValid,
        markValid,
      );

      this._invalidateCartCache = invalidate;
      this._eventEmitter = eventEmitter;

      eventEmitter.on('acfAddPromoCode', this.setPromoCode.bind(this));
      eventEmitter.on('acfRemovePromoCode', this.resetPromoCode.bind(this));

      this._updateCartDataView()
      // This is a hack. The front-end components should subscribe to
      // cart update events, but they currently don't. We do this to force
      // update of the cart once details have been loaded from the platform.
        .then(() => this._eventEmitter.emit('acfAddToCart'));
    }

    /**
     * Returns the CartItem associated with provided cartItemId.
     *
     * Returned CartItem is a clone. Modifying it from outside has no effect on
     * the internal data stored inside the cart.
     *
     * @param {CartItemId} cartItemId ID of the CartItem to return
     * @returns {CartItem} CartItem associated with provided cartItemId
     * @throws {NoSuchCartItemError} if no CartItem is associated cartItemId
     */
    async _getItemById(cartItemId) {
      const items = await this._getItems();
      const item = items.find(i => i.id === cartItemId);
      if (item === undefined) {
        throw new NoSuchCartItemError(`No cart item found with id ${cartItemId}`);
      }
      return item;
    }

    /**
     * Returns an array containing all CartItems currently in the cart.
     *
     * Returned CartItems are clones. Modifying them from outside has no effect
     * on the internal data stored inside the cart.
     *
     * @returns {CartItem[]} Array of all CartItems in cart
     */
    async _getItems() {
      const body = await this._getElasticPathCart();
      const items = body.included
        ? await Promise.all(body
          .included
          .items
          .map(transformElasticPathCartItemToAcf))
        : [];
      return items;
    }

    /**
     * Adds the specified product to cart, emits acfAddToCart and acfUpdateCart
     * events, then returns the CartItemId for the CartItem associated with the
     * added product.
     *
     * @param productId {ProductId} ID of the product to add
     * @param quantity {Quantity} Number of products to add
     * @param options {AddProductOptions} Placeholder for future optional parameters. Optional.
     * @returns {CartItemId} ID associated with the CartItem for the added product
     * @emits acfAddToCart with the ProductId of the product that was added to cart
     * @emits acfUpdateCart (no parameters)
     * @throws {NoSuchProductError} If no product was found for the productId
     * @throws {InsufficientStockError} If there wasn't enough stock for the requested amount
     */
    async addToCart(productId, quantity, options) {
      const elasticPathProduct = await elasticPathACFUtils.getElasticPathProductByNodeId(productId);
      const elasticPathProductId = elasticPathProduct.id;
      const cartId = getElasticPathCartId();

      const response = await elasticPathACFUtils.elasticPathFetch(
        `${CARTS_ENDPOINT}/${cartId}/items`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: {
              id: elasticPathProductId,
              type: 'cart_item',
              quantity: asQuantity(quantity),
            }
          }),
        },
      );

      if (!response.ok) {
        throw new Error('Unknown error', response);
      }

      // Invalidate cart cache
      this._invalidateCartCache();

      // Emit events
      await this._updateCartDataView();
      this._eventEmitter.emit(ADD_TO_CART_EVENT_KEY, productId);
    }

    /**
     * Deletes the item associated with the CartItemId, then emits
     * acfRemoveFromCart and acfUpdateCart events.
     *
     * @param cartItemId {CartItemId} ID of the item to delete
     * @emits acfRemoveFromCart event With the ProductId of the product removed
     * @emits acfUpdateCart event
     * @throws {NoSuchCartItemError} If no CartItem was found for the provided ID
     */
    async removeFromCart(cartItemId) {
      const cartId = getElasticPathCartId();
      const item = await this._getItemById(cartItemId);
      const productId = item.productId;

      const response = await elasticPathACFUtils.elasticPathFetch(
        `${CARTS_ENDPOINT}/${cartId}/items/${cartItemId}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );

      if (!response.ok) {
        throw new Error('Unknown error', response);
      }

      const json = await response.json();

      // Invalidate cart cache
      this._invalidateCartCache();

      if (json.data.length > 0) {
        await this._updateCartDataView();
      } else {
        await this.empty();
      }
      this._eventEmitter.emit(REMOVE_FROM_CART_EVENT_KEY, productId);
    }

    /**
     * Sets the quantity of the provided CartItemId to Quantity, then emits
     * acfUpdateQuantity and acfUpdateCart events. If provided quantity is 0,
     * bypasses all functionality and instead calls deleteItem with provided
     * cartItemId.
     *
     * @param cartItemId {CartItemId} ID of the CartItem to update
     * @param quantity {Quantity} Desired quantity
     * @emits acfUpdateCart event
     * @throws {NoSuchCartItemError} If no CartItem was found for the provided ID
     * @throws {InsufficientStockError} If there wasn't enough stock for the requested quantity
     */
    async updateCartProduct(cartItemId, quantity, options) {
      const cartId = getElasticPathCartId();

      const response = await elasticPathACFUtils.elasticPathFetch(
        `${CARTS_ENDPOINT}/${cartId}/items/${cartItemId}`,
        {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: {
              quantity: asQuantity(quantity),
            }
          }),
        },
      );

      if (!response.ok) {
        if (response.status === 404) {
          throw new NoSuchCartItemError(`No cart item found with id ${cartItemId}`);
        }
        throw new Error('Unknown error', response);
      }

      // Invalidate cart cache
      this._invalidateCartCache();

      await this._updateCartDataView();
    }

    /**
     * Removes all items from the cart, then emits acfEmptyCart and acfUpdateCart
     * events.
     *
     * @emits acfUpdateCart event
     * @emits acfEmptyCart event
     */
    async empty() {
      const cartId = getElasticPathCartId();

      const response = await elasticPathACFUtils.elasticPathFetch(
        `${CARTS_ENDPOINT}/${cartId}/items`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );

      if (!response.ok) {
        throw new Error('Unknown error', response);
      }

      // Invalidate cart cache
      this._invalidateCartCache();

      await this._updateCartDataView();

      // Emit events
      this._eventEmitter.emit(EMPTY_CART_EVENT_KEY);
    }

    /**
     * Returns the Product associated with the provided productId.
     *
     * The returned value is a clone. Modifying it has no impact on internal
     * details of the ACF plugin or commerce platform.
     *
     * @param id {ProductId} ProductId for the product to retrieve
     * @returns {Product} Details for the product associated with productId
     * @throws {NoSuchProductError} If no product can befound for productId
     */
    async getProduct(id) {
      const elasticPathProduct = await elasticPathACFUtils.getElasticPathProductByNodeId(id);
      const drupalNode = await elasticPathACFUtils.getDrupalProductNodeById(id);

      return {
        ...drupalNode,
        price: formatPrice(elasticPathProduct.price[0].amount),
      }
    }

    _getPromoCode() {
      return localStorage.getItem(PROMOCODE_LOCALSTORAGE_KEY) || '';
    }

    setPromoCode(promoCode) {
      localStorage.setItem(PROMOCODE_LOCALSTORAGE_KEY, promoCode);
      this._updateCartDataView();
    }

    resetPromoCode() {
      localStorage.removeItem(PROMOCODE_LOCALSTORAGE_KEY);
      this._updateCartDataView();
    }

    /**
     * Returns the Elastic Path Commerce Cloud cart.
     *
     * @returns object Elastic Path Commerce Cloud cart.
     */
    async _getElasticPathCart() {
      const cartId = getElasticPathCartId();
      const res = await elasticPathACFUtils.elasticPathFetch(
        `${CARTS_ENDPOINT}/${cartId}?include=items`
      );

      if (!res.ok) {
        throw new Error(`Could not retrieve Elastic Path cart id ${cartId}`);
      }
      return await res.json();
    }

    /**
     * Helper - write the local storage label for a product cache item. This
     *  ensures that we are controlling the product label in storage across all
     *  consumers of this library.
     *
     * NOTE: This actually gets used by visual components.
     *
     * @param {number} id
     */
    _labelProduct(id) {
      return 'acfProduct-' + id;
    }

    /**
     * Updates the publically-visible cart data, which is used for rendering
     * by various visible components.
     */
    async _updateCartDataView() {
      const products = await this._getItems();
      const cart = await this._getElasticPathCart();

      ACF.cart = {
        products: products.reduce(
          (products, product) => {
            return {
              ...products,
              [product.id]: {
                id: product.productId,
                qty: product.quantity,
                attr: {},
              },
            };
          },
          {}
        ),
        promoCode: this._getPromoCode(),
        amount: {
          subtotal: formatPrice(cart.data.meta.display_price.without_tax.amount),
          discount: '0.00',
          tax: formatPrice(cart.data.meta.display_price.tax.amount),
          shipping: '',
          total: formatPrice(cart.data.meta.display_price.with_tax.amount),
        },
      };

      this._eventEmitter.emit(CART_UPDATE_EVENT_KEY);
    };
  }
})();
