const elasticPathACFUtils = (function () {

  /**
   * @constant string Products endpoint in Elastic Path Commerce Cloud.
   */
  const PRODUCTS_ENDPOINT = '/v2/products';

  /**
   * @constant string Product API endpoint in Drupal.
   */
  const DRUPAL_PRODUCT_ENDPOINT = '/api/acf/product';

  /**
   * @constant string Endpoint for the Product search index in Drupal.
   */
  const DRUPAL_PRODUCT_SEARCH_INDEX = '/jsonapi/index/product';

  /**
   * Retrieves Commerce Cloud configuration, provided by the Elastic Path
   * Commerce Cloud ACF Config module.
   */
  const getConfig = (() => {
    // cache retrieved configuration
    // TODO: Cache this more effectively
    let config;

    return async function () {
      if (!config) {
        const res = await fetch('/api/elasticpath/commerce-cloud/config?_format=json');
        if (!res.ok) {
          throw new Error('Could not retrieve configuration');
        }

        const body = await res.json();

        config = {
          host: body.host,
          clientId: body.client_id,
        };
      }
      return config;
    }
  })();

  /**
   * Returns an Elastic Path Commerce Cloud implicit access token.
   *
   * @throws Error
   *   If authentication fails.
   *
   * @returns string
   *   Bearer token for use with Elastic Path Commerce Cloud.
   */
  const getElasticPathAccessToken = (() => {
    // TODO: Cache this more effectively
    let token;
    let expires;

    const FIVE_MINUTES = 5 * 60 * 1000;
    const SEC_TO_MS = 1000;

    return async function () {
      if (!token || expires < Date.now()) {
        const { host, clientId } = await getConfig();

        const res = await fetch(`https://${host}/oauth/access_token`, {
          method: 'POST',
          credentials: 'omit',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: '*/*',
          },
          body: `grant_type=implicit&client_id=${clientId}`,
        });

        if (!res.ok) {
          throw new Error("Couldn't authenticate");
        }

        const body = await res.json();

        token = `${body.token_type} ${body.access_token}`;
        // Subtract 5 minutes from expiry time, just to be safe
        expires = (body.expires * SEC_TO_MS) - FIVE_MINUTES;
      }

      return token;
    }
  })();

  /**
   * Makes authenticated fetch calls to Elastic Path Commerce Cloud.
   *
   * @param string path
   *   Path in the Commerce Cloud API to fetch.
   *
   * @param object options
   *   Optional extra arguments to pass to fetch.
   *
   * @returns Response
   *   Fetch response object.
   */
  async function elasticPathFetch(path, options = {}) {
    const { host } = await getConfig();
    const token = await getElasticPathAccessToken();

    return fetch(encodeURI(`https://${host}/${path}`), {
      ...options,
      headers: {
        ...(options.headers || {}),
        Authorization: token,
      }
    });
  }

  /**
   * Memoizes the provided function.
   *
   * Note that this should only be used with pure functions.
   *
   * @param function fn Function to memoize.
   * @param function isValid Returns true if cached item is valid.
   * @param function invalidate Invalidates a cached item.
   * @returns function Memoized copy of fn.
   */
  function memoize(fn, isValid = () => true, markAsValid = () => {}) {
    const RESULT = Symbol.for('__RESULT__');
    const cache = {};

    const getCachedResult = (...args) => {
      let posInTree = cache;
      for (const arg of args) {
        posInTree = posInTree[arg];
        if (posInTree === undefined) {
          return undefined;
        }
      }
      if (!isValid(posInTree)) {
        posInTree[RESULT] = undefined;
      }
      return posInTree[RESULT];
    };

    const setCachedResult = (result, ...args) => {
      let posInTree = cache;
      for (let i = 0; i < args.length; i++) {
        const arg = args[i];
        posInTree[arg] = posInTree[arg] || {};
        posInTree = posInTree[arg];
      }
      posInTree[RESULT] = result;
      markAsValid(posInTree);
    };

    return (async function(...args) {
      let cachedResult = getCachedResult(...args);
      if (cachedResult === undefined) {
        cachedResult = await fn.call(this, ...args);
        setCachedResult(cachedResult, ...args);
      }
      return cachedResult;
    });
  }


  /**
   * No product matches the provided Id.
   */
  class NoSuchProductError extends Error {
    constructor(msg) {
      super(msg);
      this.name = 'NoSuchProductError';
    }
  }

    /**
     * Returns the Drupal Product node for the provided Id.
     *
     * @returns object Drupal Product node for provided Id.
     * @throws NoSuchProductError If the product cannot be found.
     * @throws Error On all other errors.
     */
  const getDrupalProductNodeById = memoize(async function (productNodeId) {
    const res = await fetch(`${DRUPAL_PRODUCT_ENDPOINT}/${productNodeId}?_format=json`);
    if (!res.ok) {
      if (res.status === 404) {
        throw new NoSuchProductError(
          `No Drupal Product node exists with Id ${productNodeId}`,
        );
      }
      throw new Error(res);
    }
    const body = await res.json();
    return body[0];
  });

  /**
   * Returns the Elastic Path Commerce Cloud product for the provided node Id.
   *
   * @param string productNodeId The Drupal node Id for the product to fetch.
   * @returns object The Elastic Path Commerce Cloud product for the provided node Id.
   * @throws NoSuchProductError If the Drupal node Id does not exist.
   * @throws NoSuchProductError If Drupal Product node does not map to a Commerce Cloud product.
   */
  const getElasticPathProductByNodeId = memoize(async function (productNodeId) {
    // Fetch Drupal Product node, so that we can retrieve the SKU.
    const drupalProduct = await getDrupalProductNodeById(productNodeId);
    const sku = drupalProduct.sku;

    // Fetch actual product information from Elastic Path Commerce Cloud.
    const res = await elasticPathFetch(`${PRODUCTS_ENDPOINT}?filter=eq(sku, ${sku})`);
    if (!res.ok) {
      throw new Error(
        'Unknown error fetching products from Elastic Path Commerce Cloud',
        await res.json(),
      );
    }

    const body = await res.json();
    if (body.data === null || body.data.length === 0) {
      throw new NoSuchProductError(
        `No Elastic Path Commerce Cloud exists with SKU ${sku}`,
      );
    }

    return body.data[0];
  });

  /**
   * Returns the Drupal node ID for a given Elastic Path product SKU.
   *
   * @param string sku Elastic Path product SKU to map.
   * @returns string Drupal node ID associated with SKU.
   * @throws Error If the SKU does not map to a valid Drupal node ID.
   */
  const getDrupalNodeByElasticPathSku = memoize(async (sku) => {
    const response = await fetch(
      encodeURI(`${DRUPAL_PRODUCT_SEARCH_INDEX}?filter[field_product_sku]=${sku}`),
    );

    if (!response.ok) {
      throw new Error( `Could not retrieve Drupal node for Elastic Path SKU ${sku}`,
        await response.json(),
      );
    }

    const body = await response.json();
    if (body.data === undefined || body.data.length === 0) {
      throw new Error(`No Drupal node found for Elastic Path SKU ${sku}`);
    }

    return body.data[0];
  });

  /**
   * Interface for all interactions with the Utilities.
   *
   * @param EventEmitter eventEmitter ACF's event emitter.
   * @constraint At any given time, each CartItemId must map to at most one CartItem.
   */
  return {
    getConfig,
    elasticPathFetch,
    getDrupalProductNodeById,
    getElasticPathProductByNodeId,
    getDrupalNodeByElasticPathSku,
    memoize,
  };
})();
