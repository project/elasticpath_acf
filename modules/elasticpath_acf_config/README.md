# Elastic Path ACF Config

This module performs two main functions:

1. It adds Drupal [configuration](https://www.drupal.org/docs/drupal-apis/configuration-api) for configuring a **client ID** and **hostname** that the other modules will use when interacting with Elastic Path Commerce Cloud. This configuration can be managed from the UI under **Admin > Development > Elastic Path Commerce Cloud ACF Connector Settings**.
2. It provides those configuration values to the service layer using Drupal's [JSON:API](https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module/api-overview).

This module is used by:

- `elasticpath_acf_components` to enable commerce interactions in the browser.
- `elasticpath_acf_feeds` to import Elastic Path Commerce Cloud catalog data into Drupal.