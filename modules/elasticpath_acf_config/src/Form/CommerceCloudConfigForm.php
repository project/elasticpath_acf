<?php

namespace Drupal\elasticpath_acf_config\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a configuration form for the Elastic Path Commerce Cloud ACF
 * connector.
 */
class CommerceCloudConfigForm extends ConfigFormBase {

  /**
   * Key for the configuration that this form edits.
   *
   * @var string
   */
  const SETTINGS = 'elasticpath_acf_config.settings';

   /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'elasticpath_acf_config_settings';
  }

   /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $config->get('host'),
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory()->getEditable(static::SETTINGS)
      ->set('host', $form_state->getValue('host'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
