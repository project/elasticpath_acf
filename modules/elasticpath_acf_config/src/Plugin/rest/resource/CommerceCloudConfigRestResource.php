<?php

namespace Drupal\elasticpath_acf_config\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "commerce_cloud_config_rest_resource",
 *   label = @Translation("Elastic Path Commerce Cloud ACF Configuration Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/elasticpath/commerce-cloud/config"
 *   }
 * )
 */
class CommerceCloudConfigRestResource extends ResourceBase {

  /**
   * Key for the configuration that this form edits.
   *
   * @var string
   */
  const SETTINGS = 'elasticpath_acf_config.settings';

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('elasticpath_acf_config');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * Returns currently-configured Elastic Path Commerce Cloud host and
   * client_id.
   *
   * @param string $payload
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($payload) {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $config = \Drupal::config(static::SETTINGS);

    $response = new ResourceResponse([
      'host' => $config->get('host'),
      'client_id' => $config->get('client_id'),
    ], 200);

    $response->addCacheableDependency($config);

    return $response;
  }

}
