<?php

namespace Drupal\elasticpath_acf_feeds\Feeds\Fetcher;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Fetcher\HttpFetcher;
use Drupal\feeds\Result\HttpFetcherResult;
use Drupal\feeds\StateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Your Class Description.
 *
 * @FeedsFetcher(
 *   id = "elasticpath_acf_feeds_category_fetcher",
 *   title = @Translation("Elastic Path Commerce Cloud Category Fetcher"),
 *   description = @Translation("Fetches categories from Elastic Path Commerce Cloud"),
 * )
 */
class ElasticPathCategoryFetcher extends HttpFetcher {

  /**
   * Key for the Elastic Path ACF Config module.
   *
   * @var string
   */
  const SETTINGS = 'elasticpath_acf_config.settings';

  /**
   * Pagination settings for Commerce Cloud.
   *
   * @var number
   */
  const PAGE_SIZE = 25;

  /**
   * Temporary authentication token returned from Commerce Cloud.
   *
   * @var string
   */
  private $authToken;

  /**
   * Config factory for retrieving config from Elastic Path ACF Config module.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Constructs an UploadFetcher object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle client.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The Drupal file system helper.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    ClientInterface $client,
    CacheBackendInterface $cache,
    FileSystemInterface $file_system,
    ConfigFactoryInterface $config_factory
  ) {
    $this->configFactory = $config_factory;
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $client,
      $cache,
      $file_system,
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('cache.feeds_download'),
      $container->get('file_system'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    if (!isset($state->page)) {
      $state->page = 0;
    }

    $response = $this->getWithAuth($this->urlForPath('/v2/categories'), [
      RequestOptions::QUERY => [
        'page[limit]' => static::PAGE_SIZE,
        'page[offset]' => $state->page * static::PAGE_SIZE,
      ],
    ]);

    $body = json_decode($response->getBody());
    $headers = $response->getHeaders();

    // Write the results to file.
    $sink = $this->prepareSink();
    $file = fopen($sink, 'w');
    fwrite($file, $response->getBody());
    fclose($file);

    // Update state.
    $state->page += 1;
    $state->progress(
      $lastPage = $body->meta->page->total,
      $curPage = $body->meta->page->current,
    );

    return new HttpFetcherResult($sink, $headers);
  }

  /**
   * Performs a GET request.
   *
   * @param string $url
   *   The URL to GET.
   * @param array $requestOptions
   *   Array of request options to pass to GuzzleHttp client.
   *
   * @return \Guzzle\Http\Message\Response
   *   A Guzzle response.
   *
   * @throws \RuntimeException
   *   Thrown if the GET request failed.
   *
   * @see \GuzzleHttp\RequestOptions
   */
  protected function getWithAuth($url, array $requestOptions = []) {
    // Wrap in loop so that we can retry on 401 (invalid token) responses.
    for ($tries = 1; $tries < 2; $tries++) {
      $requestOptions[RequestOptions::HEADERS]['Authorization'] = $this->getAuthToken();

      try {
        $response = $this->client->get($url, $requestOptions);
        return $response;
      }
      catch (RequestException $e) {
        if ($response->getStatusCode != 401 || $tries > 1) {
          throw $e;
        }

        $this->clearAuthToken();
      }
    }
  }

  /**
   * Returns an auth token from Commerce Cloud, fetching a new one if required.
   */
  private function getAuthToken() {
    if (!isset($this->authToken)) {
      $this->authenticate();
    }
    return $this->authToken;
  }

  /**
   * Invalidates the auth token.
   */
  private function clearAuthToken() {
    unset($this->authToken);
  }

  /**
   * Authenticates with Commerce Cloud.
   *
   * Stores the result in the the private $authToken property. Uses the
   * "implicit" grant type.
   *
   * @throws RuntimeException
   *   If Commerce Cloud authentication fails.
   */
  private function authenticate() {
    $authUrl = $this->urlForPath('/oauth/access_token');
    $client_id = $this->configFactory->get(static::SETTINGS)->get('client_id');

    $response = $this->client->request('POST', $authUrl, [
      'form_params' => [
        'client_id' => $client_id,
        'grant_type' => 'implicit',
      ],
    ]);

    if ($response->getStatusCode() !== 200) {
      throw new \RuntimeException($this->t(
        'Unable to authenticate with Elastic Path Commerce Cloud. Response code %code',
        ['%code' => $response->getStatusCode()]));
    }

    $responseJson = json_decode($response->getBody());
    if (!isset($responseJson->access_token)) {
      throw new \RuntimeException($this->t(
        'Unable to authenticate with Elastic Path Commerce Cloud. Required response field access_token missing'));
    }

    if (!isset($responseJson->token_type)) {
      throw new \RuntimeException($this->t(
        'Unable to authenticate with Elastic Path Commerce Cloud. Required response field token_type missing'));
    }

    $this->authToken = "{$responseJson->token_type} {$responseJson->access_token}";
  }

  /**
   * Returns full URL for provided path, based on the current host setting.
   *
   * @param string $path
   *   Path to transform into full URL.
   *
   * @return string
   *   Full URL for provided path, based on the current host parameter
   */
  private function urlForPath(string $path) {
    $host = $this->configFactory->get(static::SETTINGS)->get('host');
    return 'https://' . $host . $path;
  }

  /**
   * Creates and returns temporary storage for storing API responses.
   *
   * @return string
   *   Location of temporary storage for storing API responses.
   */
  private function prepareSink() {
    $sink = $this->fileSystem->tempnam('temporary://', 'feeds_http_fetcher');
    $sink = $this->fileSystem->realpath($sink);
    return $sink;
  }

}
